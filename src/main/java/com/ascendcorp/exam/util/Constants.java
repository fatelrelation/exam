package com.ascendcorp.exam.util;

public class Constants {
    public static final String RESPONSE_CODE_APPROVED = "approved";
    public static final String RESPONSE_CODE_INVALID_DATA = "invalid_data";
    public static final String RESPONSE_CODE_TRANSACTION_ERROR = "transaction_error";
    public static final String RESPONSE_CODE_UNKNOWN = "unknown";

    public static final String ERROR_MESSAGE_INVALID_DATA = "General Invalid Data";
    public static final String ERROR_MESSAGE_TRANSACTION_ERROR = "General Transaction Error";
    public static final String ERROR_MESSAGE_TIMEOUT = "Error timeout";
    public static final String ERROR_MESSAGE_APPLICATION_ERROR = "Internal Application Error";

    public static final String WEB_SERVICE_EXCEPTION_SOCKET_TIMEOUT = "java.net.SocketTimeoutException";
    public static final String WEB_SERVICE_EXCEPTION_TIMEOUT = "Connection timed out";
}
