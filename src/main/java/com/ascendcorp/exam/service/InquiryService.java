package com.ascendcorp.exam.service;

import javax.xml.ws.WebServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.ascendcorp.exam.model.InquiryServiceRequestDTO;
import com.ascendcorp.exam.model.InquiryServiceResultDTO;
import com.ascendcorp.exam.model.TransferRequest;
import com.ascendcorp.exam.model.TransferResponse;
import com.ascendcorp.exam.proxy.BankProxyGateway;
import com.ascendcorp.exam.util.Constants;

public class InquiryService {

    @Autowired
    private BankProxyGateway bankProxyGateway;

    final static Logger log = Logger.getLogger(InquiryService.class);

    public InquiryServiceResultDTO inquiry(InquiryServiceRequestDTO inquiryServiceRequestDTO) {
        InquiryServiceResultDTO respDTO = null;
        try {
            log.info("validate request parameters.");
            boolean isReqParamError = inputValidation(inquiryServiceRequestDTO);
            if (isReqParamError) {
                respDTO = setReasonCodeAndDesc(respDTO, "500", Constants.ERROR_MESSAGE_INVALID_DATA);
            } else {
                log.info("call bank web service");
                TransferRequest request = new TransferRequest();
                BeanUtils.copyProperties(inquiryServiceRequestDTO, request);
                TransferResponse response = bankProxyGateway.requestTransfer(request);

                log.info("check bank response code");
                respDTO = checkResponseTransfer(response);
            }
        } catch (WebServiceException exception) {
            respDTO = handleWebServiceException(respDTO, exception);
        } catch (Exception exception) {
            respDTO = handleInternalApplicationError(respDTO, exception);
        }
        return respDTO;
    }

    private boolean inputValidation(InquiryServiceRequestDTO inquiryServiceRequestDTO) {
        boolean isError = false;
        if (inquiryServiceRequestDTO.getTransactionId() == null) {
            log.info("Transaction id is required!");
            isError = true;
        }
        if (inquiryServiceRequestDTO.getTranDateTime() == null) {
            log.info("Transaction DateTime is required!");
            isError = true;
        }
        if (inquiryServiceRequestDTO.getChannel() == null) {
            log.info("Channel is required!");
            isError = true;
        }
        if (inquiryServiceRequestDTO.getBankCode() == null
                || inquiryServiceRequestDTO.getBankCode().equalsIgnoreCase("")) {
            log.info("Bank Code is required!");
            isError = true;
        }
        if (inquiryServiceRequestDTO.getBankNumber() == null
                || inquiryServiceRequestDTO.getBankNumber().equalsIgnoreCase("")) {
            log.info("Bank Number is required!");
            isError = true;
        }
        if (inquiryServiceRequestDTO.getAmount() <= 0) {
            log.info("Amount must more than zero!");
            isError = true;
        }

        return isError;
    }

    private InquiryServiceResultDTO checkResponseTransfer(TransferResponse response) {
        InquiryServiceResultDTO respDTO = null;
        if (response != null) {
            log.debug("found response code");
            respDTO = new InquiryServiceResultDTO();
            respDTO.setRef_no1(response.getReferenceCode1());
            respDTO.setRef_no2(response.getReferenceCode2());
            respDTO.setAmount(response.getBalance());
            respDTO.setTranID(response.getBankTransactionID());

            if (response.getResponseCode().equalsIgnoreCase(Constants.RESPONSE_CODE_APPROVED)) {
                respDTO = setResponseResultCaseApprove(respDTO, response);
            } else if (response.getResponseCode().equalsIgnoreCase(Constants.RESPONSE_CODE_INVALID_DATA)) {
                respDTO = setResponseResultCaseInvalidData(respDTO, response);
            } else if (response.getResponseCode().equalsIgnoreCase(Constants.RESPONSE_CODE_TRANSACTION_ERROR)) {
                respDTO = setResponseResultCaseTransactionError(respDTO, response);
            } else if (response.getResponseCode().equalsIgnoreCase(Constants.RESPONSE_CODE_UNKNOWN)) {
                respDTO = setResponseResultCaseUnknown(respDTO, response);
            } else {
                // bank code not support
                respDTO = handleInternalApplicationError(respDTO, new Exception("Unsupport Error Reason Code"));
            }
        } else {
            // no response from bank
            respDTO = handleInternalApplicationError(respDTO, new Exception("Unable to inquiry from service."));
        }
        return respDTO;
    }

    private InquiryServiceResultDTO setResponseResultCaseApprove(InquiryServiceResultDTO respDTO, TransferResponse response) {
        // bank response code = approved
        respDTO = setReasonCodeAndDesc(respDTO, "200", response.getDescription());
        respDTO.setAccountName(response.getDescription());
        return respDTO;
    }

    private InquiryServiceResultDTO setResponseResultCaseInvalidData(InquiryServiceResultDTO respDTO, TransferResponse response) {
        // bank response code = invalid_data
        String replyDesc = response.getDescription();
        if (replyDesc != null) {
            String respDesc[] = replyDesc.split(":");
            if (respDesc != null && respDesc.length >= 3) {
                // bank description full format
                respDTO = setReasonCodeAndDesc(respDTO, respDesc[1], respDesc[2]);
            } else {
                // bank description short format
                respDTO = setReasonCodeAndDesc(respDTO, "400", Constants.ERROR_MESSAGE_INVALID_DATA);
            }
        } else {
            // bank no description
            respDTO = setReasonCodeAndDesc(respDTO, "400", Constants.ERROR_MESSAGE_INVALID_DATA);
        }
        return respDTO;
    }

    private InquiryServiceResultDTO setResponseResultCaseTransactionError(InquiryServiceResultDTO respDTO, TransferResponse response) {
        // bank response code = transaction_error
        String replyDesc = response.getDescription();
        if (replyDesc != null) {
            String respDesc[] = replyDesc.split(":");
            if (respDesc != null && respDesc.length >= 2) {
                log.info("Case Inquiry Error Code Format Now Will Get From [0] and [1] first");
                log.info("index[0] : " + respDesc[0] + " index[1] is >> " + respDesc[1]);
                if ("98".equalsIgnoreCase(respDesc[0]) || respDesc.length == 2) {
                    // bank code 98 or length = 2
                    respDTO = setReasonCodeAndDesc(respDTO, respDesc[0], respDesc[1]);
                } else if (respDesc.length >= 3) {
                    log.info("case error is not 98 code and length >= 3");
                    // bank description full format
                    log.info("index[2] : " + respDesc[2]);
                    respDTO = setReasonCodeAndDesc(respDTO, respDesc[1], respDesc[2]);
                }
            } else {
                // bank description incorrect format
                respDTO = setReasonCodeAndDesc(respDTO, "500", Constants.ERROR_MESSAGE_TRANSACTION_ERROR);
            }
        } else {
            // bank no description
            respDTO = setReasonCodeAndDesc(respDTO, "500", Constants.ERROR_MESSAGE_TRANSACTION_ERROR);
        }
        return respDTO;
    }

    private InquiryServiceResultDTO setResponseResultCaseUnknown(InquiryServiceResultDTO respDTO, TransferResponse response) {
        String replyDesc = response.getDescription();
        if (replyDesc != null) {
            String respDesc[] = replyDesc.split(":");
            if (respDesc != null && respDesc.length >= 2) {
                // bank description full format
                respDTO = setReasonCodeAndDesc(respDTO, respDesc[0], respDesc[1]);
                if (respDTO.getReasonDesc() == null
                        || respDTO.getReasonDesc().trim().length() == 0) {
                    respDTO.setReasonDesc(Constants.ERROR_MESSAGE_INVALID_DATA);
                }
            } else {
                // bank description short format
                respDTO = setReasonCodeAndDesc(respDTO, "501", Constants.ERROR_MESSAGE_INVALID_DATA);
            }
        } else {
            // bank no description
            respDTO = setReasonCodeAndDesc(respDTO, "501", Constants.ERROR_MESSAGE_INVALID_DATA);
        }
        return respDTO;
    }

    private InquiryServiceResultDTO handleWebServiceException(InquiryServiceResultDTO respDTO, WebServiceException exception) {
        // handle error from bank web service
        String faultString = exception.getMessage();
        if (respDTO == null) {
            respDTO = new InquiryServiceResultDTO();
            if (isErrorTimeout(faultString)) {
                // bank timeout
                respDTO = setReasonCodeAndDesc(respDTO, "503", Constants.ERROR_MESSAGE_TIMEOUT);
            } else {
                // bank general error
                respDTO = setReasonCodeAndDesc(respDTO, "504", Constants.ERROR_MESSAGE_APPLICATION_ERROR);
            }
        }
        return respDTO;
    }

    private InquiryServiceResultDTO handleInternalApplicationError(InquiryServiceResultDTO respDTO, Exception exception) {
        log.error("inquiry exception", exception);
        if (respDTO == null || (respDTO != null && respDTO.getReasonCode() == null)) {
            respDTO = setReasonCodeAndDesc(respDTO, "504", Constants.ERROR_MESSAGE_APPLICATION_ERROR);
        }
        return respDTO;
    }

    private boolean isErrorTimeout(String faultString) {
        return faultString != null && (faultString.indexOf(Constants.WEB_SERVICE_EXCEPTION_SOCKET_TIMEOUT) > -1
                || faultString.indexOf(Constants.WEB_SERVICE_EXCEPTION_TIMEOUT) > -1);
    }

    private InquiryServiceResultDTO setReasonCodeAndDesc(InquiryServiceResultDTO respDTO, String reasonCode, String reasonDesc) {
        if (respDTO == null) {
            respDTO = new InquiryServiceResultDTO();
        }
        if (respDTO.getReasonCode() == null) {
            respDTO.setReasonCode(reasonCode);
            respDTO.setReasonDesc(reasonDesc);
        }
        return respDTO;
    }
}
