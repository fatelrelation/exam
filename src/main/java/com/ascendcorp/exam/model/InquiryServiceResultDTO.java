package com.ascendcorp.exam.model;

import java.io.Serializable;

public class InquiryServiceResultDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private String tranID;

    private String namespace;

    private String reasonCode;

    private String reasonDesc;

    private String balance;

    private String ref_no1;

    private String ref_no2;

    private String amount;

    private String accountName = null;

    public String getTranID() {
        return tranID;
    }

    public void setTranID(String tranID) {
        this.tranID = tranID;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonDesc() {
        return reasonDesc;
    }

    public void setReasonDesc(String reasonDesc) {
        this.reasonDesc = reasonDesc;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRef_no1() {
        return ref_no1;
    }

    public void setRef_no1(String ref_no1) {
        this.ref_no1 = ref_no1;
    }

    public String getRef_no2() {
        return ref_no2;
    }

    public void setRef_no2(String ref_no2) {
        this.ref_no2 = ref_no2;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Override
    public String toString() {
        return "InquiryServiceResultDTO [tranID=" + tranID + ",namespace = " + namespace
                + ", reasonCode=" + reasonCode + ", reasonDesc=" + reasonDesc + ", balance="
                + balance + ", ref_no1=" + ref_no1 + ", ref_no2=" + ref_no2 + ", amount=" + amount
                + " ,account_name=" + accountName + "  ]";
    }



}
