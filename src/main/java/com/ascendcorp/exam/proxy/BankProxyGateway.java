package com.ascendcorp.exam.proxy;

import com.ascendcorp.exam.model.TransferRequest;
import com.ascendcorp.exam.model.TransferResponse;


public class BankProxyGateway {

    public TransferResponse requestTransfer(TransferRequest requestTransferDTO) {

        return new TransferResponse();
    }
}

